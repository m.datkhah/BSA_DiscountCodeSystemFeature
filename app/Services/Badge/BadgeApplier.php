<?php
namespace App\Services\Badge;
use App\Userstate;

class BadgeApplier{

    public function apply(Userstate $userState)
    {
      $networkHandler=resolve(NetworkHandler::class);
      $softwareHandler=resolve(SoftwareHandler::class);
      $hardwareHandler=resolve(HardwareHandler::class);
      $networkHandler->setNext( $softwareHandler)->setNext($hardwareHandler);
      $networkHandler->handle($userState);
    }
}
