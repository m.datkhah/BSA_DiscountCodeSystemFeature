<?php

namespace App\Services\Badge;



use App\Badge;
use App\Userstate;

class NetworkHandler extends AbstractHandler  {

    public function handle(Userstate $userState)
    {
        if ($userState->isDirty('questionNetwork_count')){
            $this->applayBadge($userState);
        }
        return parent::handle($userState);
    }



    public function applayBadge($userState)
    {

         $availabelBadge=Badge::network()->where('required_number','<=',$userState->questionNetwork_count)->get();
         $userBadge=$userState->user->badges;
         $notRecievedBadge=$availabelBadge->diff($userBadge);

         if ( $notRecievedBadge->isEmpty()){return ;}
        $userState->user->badges()->attach($notRecievedBadge);
    }

}
