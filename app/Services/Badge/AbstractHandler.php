<?php

namespace App\Services\Badge;


 use App\Userstate;

 abstract class AbstractHandler implements Handler
 {
  private $nextHandler;

     public function setNext( Handler $handler)
     {
         $this->nextHandler=$handler;
         return $handler;
  }

     public function handle(Userstate $userstate)
     {
         if ($this->nextHandler){
             return $this->nextHandler->handle($userstate);
         }
         return  null;
  }


 }
