<?php
namespace App\Services\Traits;

use App\coupon;
use Carbon\Carbon;

trait couponable{

    public function coupons()
    {
        return $this->morphmany(coupon::class,'couponable');
    }

    public function validCoupon()
    {
        return $this->coupons()->where('expire_time','>',carbon::now());
    }
}
