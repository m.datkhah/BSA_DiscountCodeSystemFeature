<?php
namespace App\Services\Storage\Contracts;

use App\coupon;

class canUseIt extends AbstractCouponValidator{

    public function setNextValidator()
    {
        // TODO: Implement setNextValidator() method.
    }
    public function validate(coupon $coupon)
    {
      if (! auth()->user()->coupons()->contains($coupon)){
          throw new IllegalCouponExceptoin();
      }
      return parent::validate($coupon);
    }
}
