<?php

namespace App\Services\Storage\Contracts;

use App\coupon;

interface  couponValidatorInterfce {

    public function setNextValidator();

    public function validate(coupon $coupon);
}
