<?php
namespace App\Services\Storage\Contracts;

use App\coupon;

Abstract class AbstractCouponValidator implements  couponValidatorInterfce {

    private $nextValidator;

    public function NextValidator(couponValidatorInterfce $validator)
    {
        $this->nextValidator=$validator;
    }

    public function validate(coupon $coupon)
    {
        if ($this->nextValidator==null){
            return true;
        }
        return $this->nextValidator->validate($coupon);
    }

}
