<?php
namespace App\Services\Storage\Contracts;

use App\coupon;
use App\Exceptions\couponHasExpiredException;

class IsExpired extends AbstractCouponValidator
{

    public function setNextValidator()
    {
        // TODO: Implement setNextValidator() method.
    }

    public function validate(coupon $coupon)
    {
      if ($coupon->isExpired()){
          throw new couponHasExpiredException();
      }
      return parent::validate($coupon);
    }
}
