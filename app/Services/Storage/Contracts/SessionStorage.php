<?php
namespace App\Services\Storage\Contracts;

use App\Services\Storage\Contracts\storageInterface;
use Countable;

class SessionStorage implements storageInterface,Countable {

    private $bucket;
    public function __construct($bucket='default')
    {
           $this->bucket=$bucket;
    }

    public function set($index, $value)
    {
        return session()->put($this->bucket.".".$index,$value);
    }

    public function get($index)
    {
      return session()->get($this->bucket.".".$index);
    }

    public function all()
    {
        //return session()->get($this->bucket)
        return session()->get($this->bucket)??[];
    }

    public function exists($index)
    {
        return session()->has($this->bucket.".".$index);
    }

    public function unset($index)
    {
        return session()->forget($this->bucket.".".$index);
    }

    public function clear()
    {
        return session()->get($this->bucket);
    }

    public function count()
    {
        return count($this->all()) ;
    }
}
