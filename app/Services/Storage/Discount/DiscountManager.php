<?php

namespace App\Services\Storage\Discount;

use App\Services\Storage\Basket\Basket;
use App\Services\Storage\Cost\BasketCost;
use App\Services\Storage\Cost\DiscountCalculator;

class DiscountManager {

    private $discountCalculator;
    private $basketCost;
    public function __construct(BasketCost $basketCost, DiscountCalculator $discountCalculator)
    {

        $this->discountCalculator=$discountCalculator;
        $this->basketCost=$basketCost;
    }
    public function calculateUserDiscount()
    {
        if (!session()->has('coupon')){return 0;}
       return $this->discountCalculator->discountAmount(session()->get('coupon'), $this->basketCost->getTotalCosts());
    }
}
