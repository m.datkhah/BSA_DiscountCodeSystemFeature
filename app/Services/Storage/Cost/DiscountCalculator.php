<?php
namespace App\Services\Storage\Cost;

use App\coupon;

class DiscountCalculator{


    public function discountedPrice(coupon $coupons,$amount)
    {
        dd($coupons);
        return $amount - $this->discountAmount($coupons,$amount);
    }

    public function discountAmount(coupon $coupon,int $amount)
    {

        $discountAmount=(int)(($coupon->percent/100)*$amount);
        //dump($discountAmount);
        return $this->isExceeded($discountAmount,$coupon->limit )?$coupon->limit:$discountAmount;
    }

    public function isExceeded($discountAmount, int $limit)
    {
//dump($discountAmount > $limit);
        return $discountAmount > $limit;
    }
}
