<?php

namespace App\Services\Storage\Cost;

use App\Services\Storage\Contracts\costInterface;
use App\Services\Storage\Discount\DiscountManager;

class  DiscountCost implements costInterface{

    private $cost;
    private $discountManager;
    public function __construct(costInterface $cost,DiscountManager $discountManager)
    {

        $this->cost=$cost;
        $this->discountManager=$discountManager;
    }
    public function getCost()
    {
       return $this->discountManager->calculateUserDiscount();

    }
    public function getTotalCosts()
    {
               //dump($this->getCost());
        return  $this->cost->getTotalCosts() - $this->getCost();
    }

    public function persianDescription()
    {
      return 'میزان تخفیف';
    }



    public function getsummary()
    {
        return array_merge( $this->cost->getsummary(),[$this->persianDescription()=>$this->getCost()]);
    }
}
