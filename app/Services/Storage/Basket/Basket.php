<?php

namespace App\Services\Storage\Basket;

use App\Exceptions\QuantityExceededException;
use App\product;
use App\Services\Storage\Contracts\storageInterface;
use mysql_xdevapi\Exception;

class Basket {

private $storage;
    public function __construct(storageInterface $storage)
    {
      $this->storage=$storage;
    }

    public function itemCount()
    {
      return  $this->storage->count();
    }

    public function add(product $product,int $quantity)
    {
        if ($this->has($product)){
            $quantity=$this->get($product)['quantity']+$quantity;
            $this->update($product,$quantity);
        }
       return $this->storage->set($product->id,['quantity'=>$quantity]);
    }

    public function update(product $product,$quantity)
    {
        if (!$product->hasProduct($quantity)){
           throw new QuantityExceededException();
        }
        if (!$quantity){
            return $this->storage->unset($product->id);
        }
        return $this->storage->set($product->id,['quantity'=>$quantity]);

    }
    public function has($product)
    {
        return $this->storage->exists($product->id);
    }

    public function get($product)
    {
        return $this->storage->get($product->id);
    }

    public function all()
    {
      $products=product::find(array_keys($this->storage->all()));
      //dd($product);
        foreach ($products as $product){
            $product->quantity=$this->get($product)['quantity'];

        }
       // dd($products);
        return $products;
    }

    public function subTotal()
    {

       $total=0;
       foreach ($this->all() as $item){

          $total+=$item->price * $item->quantity;

       }
        return $total;
    }

}
