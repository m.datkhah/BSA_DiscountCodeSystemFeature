<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class coupon extends Model
{
    public function isExpired()
    {
      return carbon::now()->isAfter(carbon::parse($this->expire_time));
    }
}
