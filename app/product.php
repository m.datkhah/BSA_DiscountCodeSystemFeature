<?php

namespace App;

use App\Services\Storage\Cost\DiscountCalculator;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{

    public function hasProduct(int $quantity)
    {
        return $this->stock >=$quantity;
    }

    public function category()
    {
        return $this->belongsTo(category::class);
    }

    public function getPriceAttribute($price)
    {

        $coupons=$this->category->validCoupon();

        if ($coupons) {
            $discountCalculator = resolve(DiscountCalculator::class);
            return $discountCalculator->discountedPrice($coupons, $price);
        }
        return $price;
    }
}
