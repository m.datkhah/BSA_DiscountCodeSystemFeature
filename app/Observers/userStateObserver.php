<?php

namespace App\Observers;

use App\Services\Badge\BadgeApplier;
use App\Userstate;

class userStateObserver
{
    /**
     * Handle the userstate "created" event.
     *
     * @param  \App\Userstate  $userstate
     * @return void
     */
    public function created(Userstate $userstate)
    {
        //
    }

    /**
     * Handle the userstate "updated" event.
     *
     * @param  \App\Userstate  $userstate
     * @return void
     */
    public function updated(Userstate $userstate)
    {
     resolve(BadgeApplier::class)->apply($userstate);
    }

    /**
     * Handle the userstate "deleted" event.
     *
     * @param  \App\Userstate  $userstate
     * @return void
     */
    public function deleted(Userstate $userstate)
    {
        //
    }

    /**
     * Handle the userstate "restored" event.
     *
     * @param  \App\Userstate  $userstate
     * @return void
     */
    public function restored(Userstate $userstate)
    {
        //
    }

    /**
     * Handle the userstate "force deleted" event.
     *
     * @param  \App\Userstate  $userstate
     * @return void
     */
    public function forceDeleted(Userstate $userstate)
    {
        //
    }
}
