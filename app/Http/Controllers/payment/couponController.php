<?php

namespace App\Http\Controllers\payment;

use App\coupon;
use App\Http\Controllers\Controller;
use App\Services\Storage\coupon\couponValidator;
use Illuminate\Http\Request;

class couponController extends Controller
{
    private $validator;
    public function __construct(couponValidator $validator)
    {
        $this->middleware('auth');
       $this->validator=$validator;
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
               'coupon'=>['required','exists:coupons,code']
            ]);
           $coupon=coupon::where('code',$request->coupon)->firstorfail();
            $this->validator->isValid($coupon);
            session()->put(['coupon'=>$coupon]);
            return redirect()->back()->withSuccess('کد تخفیف با موفقیت اعمال شد.');

        }catch (\Exception $e){
           return redirect()->back()->withError('کد تخفیف نا معتبر است.');
        }
    }

    public function remove()
    {
        session()->forget('coupon');
        return back();
    }
}
