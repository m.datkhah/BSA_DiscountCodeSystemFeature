<?php

namespace App\Http\Controllers\payment;

use App\Exceptions\QuantityExceededException;
use App\Http\Controllers\Controller;
use App\product;
use App\Services\Storage\Basket\Basket;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    private $basket;
    public function __construct(Basket $basket)
    {
      $this->basket=$basket;
    }

    public function add(product $product)
    {
     try{
         $this->basket->add($product,1) ;
         return back()->with('successSubmit','payment.add to basket');
     }catch (QuantityExceededException $e){
         return back()->with('error',__('payment.quantityExceeded'));
     }
    }

    public function update(product $product,Request $request)
    {

        $this->basket->update($product, $request->quantity);
        return back();
    }

    public function index()
    {
       $items=$this->basket->all();
       return view('products.basket',compact('items'));
    }
}
