<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
   protected $fillable=['title','description'];

    public function scopeNetwork($query)
    {
        $query->where('type',11);
    }
      public function scopeSoftware($query)
    {
        $query->where('type',1111);
    }
    public function scopeHardware($query)
    {
        $query->where('type',111111);
    }

}

