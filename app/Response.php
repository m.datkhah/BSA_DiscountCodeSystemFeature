<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
   protected $fillable=['user_id','question_id', 'Short answer', 'text'];

    public function questions()
    {
      return  $this->belongsTo(Question::class);
   }
    public function user()
    {
      return  $this->belongsTo(User::class);
   }

}
