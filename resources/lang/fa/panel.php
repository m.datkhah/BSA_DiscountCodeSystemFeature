<?php
return [
    'users' => 'کاربرها',
    'title'=>'عنوان',
    'view list users'=>'مشاهده لیست کاربرها',
    'Add role/user access'=>'افزودن نقش  به کاربر',
    'there are not any role'=>'نقشی در سیستم وجود ندارد.',
    'there are not any role'=>'دسترسی در سیستم وجود ندارد.',
    'View and add roles'=>'بخش مشاهده و افزودن نقش',
    'Edit role'=>'ویرایش نقش',
    'My checklists'=>'چک لیست های من',
    'NotCompleted'=>'تکمیل نشده',
    'AddChecklists'=>'افزودن',
    'AddResponse'=>'افزودن پاسخ',
    'Dashboard'=>'داشبورد',
    'Search..'=>'جستجو کنید...',
    'software'=>'نرم افزار',
    'hardware'=>'سخت افزار',
    'network'=>'شبکه',
    'Subject'=>'متن',
    'home'=>'خانه',
    'categoryAttack'=>'دسته بندی حمله ها',
    'PhishingAttack'=>'حمله فیشینگ',
    'Consensus algorithm'=>'الگوریتم اجماع',
    'Wallet attack'=>'حمله کیف پول',
    'successSubmit'=>'سوال با موفقیت ثبت شد.',
    'yes'=>'بله',
    'no'=>'خیر',
    'add badge'=>'اضافه کردن badge',
    'description'=>'توضیحات را وارد کنید..',


];
