<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center" style="z-index:100">

    <div class="container">
        <div class="row ">
            <div class=" col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center ">
                <h1 class="font-type">@lang('navbar.QA_TITLE')</h1>
                <h2 class="font-type">@lang('navbar.QA_TITLE_DESCRIPTION')</h2>
                <div class="d-flex font-type">
                    <a href="#about" class="btn-get-started scrollto ">@lang('navbar.start')</a>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img">
                @include('partials.slideshow')
            </div>
        </div>
    </div>

</section>
<!-- End Hero -->
