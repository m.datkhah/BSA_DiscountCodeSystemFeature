<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center justify-content-between">
        <nav id="navbar" class="navbar font-type">
            <ul>
                <li><a class="nav-link scrollto active" href="http://127.0.0.1:8000"> @lang('navbar.home')</a></li>
                <li><a class="nav-link scrollto " href="">@lang('navbar.aboutChecklist')</a></li>
                <li class="dropdown"><a href=""><span>@lang('navbar.Checklists')</span><img
                            src="{{asset('chevron-compact-down.svg')}}" height="12px" width="12px"
                            style="height:10px;width:10px"/></a>
                    <ul>
                        <li><a href="">@lang('navbar.ChecklistsNetwork')</a></li>
                        <li><a href="">@lang('navbar.ChecklistsSoftware')</a></li>
                        <li><a href="">@lang('navbar.ChecklistsHardware')</a></li>
                    </ul>
                </li>
                <li><a class="nav-link scrollto" href>@lang('navbar.about us')</a></li>
                @auth
                    <li class="dropdown" style="margin-right: 626px">
                        <a href=""><span> {{Auth::user()->name}}</span><img
                                src="{{asset('chevron-compact-down.svg')}}" height="12px" width="12px"
                                style="height:10px;width:10px"/></a>
                        <ul>
                            <li><a href="{{route('Auth.show.profile')}}"><span> @lang('navbar.profile')</span></a></li>
                            <li><a class="dropdown-item" href="{{route('Auth.logout')}}">@lang('navbar.logout')</a></li>
                        </ul>
                    </li>
                @endauth

                @guest
                    <li><a class="btn btn-light  mr-2" style="margin-right: 525px;height: 47px"
                           href="{{route('Auth.showLoginForm')}}">@lang('navbar.login')</a>
                        <a class="btn btn-light mr-2" style="margin-right: 572px;margin-top: -46px"
                           href="{{route('Auth.show.register.form')}}">@lang('navbar.register')</a>
                    </li>
                @endguest
            </ul>
        </nav>
    </div>
</header>

