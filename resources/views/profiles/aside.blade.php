<ul class="sidebar-menu" style="direction: rtl;margin-right: 18px;">
    <li class="active">
        <a class="" href="{{route('Auth.show.checklist')}}">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-check"
                 viewBox="0 0 16 16" style="margin-right: 7px;">
                <path fill-rule="evenodd"
                      d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
            </svg>
            <span>@lang('panel.My checklists')</span>
        </a>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" class="">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bell-fill"
                 viewBox="0 0 16 16" style="margin-right: 7px;">
                <path
                    d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z"/>
            </svg>
            <span>@lang('notifications.notification')</span>
            <span style="float: left" class="menu-arrow arrow_carrot-left"></span>
        </a>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" class="">
            <i class="icon_desktop"></i>
            <span>@lang('panel.users')</span>
            <span style="float: left" class="menu-arrow arrow_carrot-left"></span>
        </a>
        <ul class="sub">
            <li><a class="" href="{{route('app.new.badge')}}">@lang('panel.add badge')</a></li>
        </ul>
    </li>
</ul>


