One of the advantages of store sites is its shopping cart section, which has made store sites more and more popular.
The BSA project is a prototype designed and implemented for laboratory sections. We plan to turn this project into a store site. As mentioned, one of the most important parts of a shopping site is its shopping cart. In this project, features such as discount code system, invoice issuance system and payment system have been considered for the shopping cart and each of them has been explained in detail in separate sections. 
To better articulate the subject, consider 4 blocks (systems). Those blocks are in the following order:
1) Static system (Basket.php)
2) Dynamic system (BasketCost.php)
3) Shipping cost system or applying shipping cost to shopping cart (ShippingCost.php)
4) Discount code system to shopping cart (DiscountCost.php)
Before the full expression of these 4 blocks, a contract is defined and the classes of each block follow this contract. In this contract 4 functions are placed as follows:
- method getcost (): Returns the entire cost of the shopping cart.
- method getTotalcost (): The cost of a shopping cart plus or minus items.
- method persionDiscription (): The name of the item is stated.
- method getSummary (): In the form of a presentation and in the form of index and value, the second and third methods are placed, respectively.
1. Block 1: In the shopping cart page, we have a section called payment, and in this section, we have entered the shipping cost manually, and now, if we want to apply the discount code, we will run into problems, so first, from the hard code to the dynamic We convert.

2. Block 2: To dynamize the BasketCost class implement costInterface.
costInterface includes the 4 functions described above.
In this block, it should be noted that the sample created from the Basket class is called in the getcost method to access the total cost of the shopping cart in the previous block.
Note that this is done to show the cost of the shopping cart and shipping costs in the user interface
method getsummary key, value The previous block inside this method is passed along with the key, value of this block, but the array_mearge method is used to convert these two arrays into one array.


3. Block 3: class ShippingCost implement costInterface.
In this block, it should be noted that the sample created from the Basketcost class is passed into this class to add to the cost of the shopping cart shipping cost.


4. block 4:
Before stating this block, it should be noted that the discount code is to be applied, for example, to a specific user or to a set of a specific category of products.
In this section, the coupon model is to be assigned to the user, catagory product model, so a polymorphic relationship is required

- The first assumption is that the coupon is to be assigned to a specific user. Therefore, in the user interface section, there is a section in which it is checked if a discount code is stored in the user's session to show it to the user, otherwise a form is placed so that the user can enter his discount code. Slowly
 In the backend section, in the first step, it should be checked whether the discount code is valid from the perspective of time and in the next step, whether the user can use this discount code.
Therefore, these two steps must be performed sequentially, for which the chain of responsibility feature has been used.
After confirming both chains 
 We create class DiscountManger to calculate the discount code through this class, at the end, an example of this class is called in DiscountCost.php 

- Second assumption
The discount code is applied to a specific category of products.
In this section, accessories andMaintors are used.  Pay attention to this example to express the use of these features. Assuming you want to get a product, this feature first checks the existence of the discount code and, if any, applies at the end of the product.



